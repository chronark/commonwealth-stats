import json
import os
import time

import requests
from bs4 import BeautifulSoup


def load_json(filename):
    with open(filename, 'r') as file:
        return json.load(file)


def save_json(json_data, filename):
    with open(filename, 'w') as file:
        json.dump(json_data, file, indent=4, sort_keys=True)


def get_url(charter_code):
    commonwealth_base_url = 'https://www.my-colony.com/colonies/'
    return commonwealth_base_url + charter_code


class Downloader:
    def __init__(self, common_wealth_charter_code):
        self.common_wealth_charter_code = common_wealth_charter_code
        self.file = '{}.json'.format(self.common_wealth_charter_code)
        colonies, rrr = self.download_data()
        self.save_data(colonies, rrr)

    def download_data(self):
        response = requests.get(get_url(self.common_wealth_charter_code))
        if response.status_code != 200:
            raise ConnectionError('Could not reach {}'.format(response.url))
        html = response.text
        soup = BeautifulSoup(html, 'lxml')
        html_colonies = soup.find_all(name='div', attrs={'class': 'commonwealthItem'})
        rrr = soup.find(name='div', attrs={'id': 'rrrIndexArea'}).text[12:].strip()
        colonies = []
        for colony in html_colonies:
            soup = BeautifulSoup(str(colony), 'lxml')
            name = soup.find(name='div', attrs={'class': 'commonwealthItemName'}).text
            charter_code = soup.find(name='a', href=True)['href'][10:-1]
            population = soup.find_all(name='div', attrs={'class': 'commonwealthItemPopulation'})[0].text[12:]
            gdp = soup.find_all(name='div', attrs={'class': 'commonwealthItemPopulation'})[1].text[5:]
            colonies.append({'name': name, 'charter_code': charter_code, 'population': population, 'gdp': gdp})
        return colonies, rrr

    def save_data(self, colonies, rrr):
        if os.path.isfile(self.file):
            save_data = load_json(self.file)
        else:
            save_data = []
        new_data = {
            'time_stamp': int(time.time()),
            'colonies'  : colonies,
            'rrr'       : rrr
        }
        save_data.append(new_data)
        save_json(save_data, self.file)


class Table:
    def __init__(self, common_wealth_charter_code, table_length=10):
        self.common_wealth_charter_code = common_wealth_charter_code
        self.read_file = '{}.json'.format(self.common_wealth_charter_code)
        self.write_file = 'forum.txt'
        self.remove_file()
        self.table_length = table_length
        self.data = load_json(self.read_file)
        self.latest, self.earlier = self.get_latest()
        self.raw_table = self.construct_table()
        self.head_raw_table = self.raw_table[:self.table_length + 1]
        self.formatted_table = self.format_table()

    def get_colony_url(self, name):
        for colony in self.latest['colonies']:
            if name.strip() == colony['name']:
                return get_url(colony['charter_code'])
        else:
            print('no colony found')

    def get_order(self, colonies):
        order = []
        for colony in colonies:
            order.append(colony['name'])
        return order

    def construct_table(self):
        colonies_now = self.latest['colonies']
        colonies_earlier = self.earlier['colonies']
        order_now = self.get_order(colonies_now)
        try:
            colonies_earlier = self.earlier['colonies']
            order_earlier = self.get_order(colonies_earlier)
        except:
            pass
        table = []
        table.append(['Name', 'Population', 'GDP', 'Change', 'Status'])
        for colony in colonies_now:
            name = colony['name']
            try:
                rank_now = order_now.index(name)
                rank_earlier = order_earlier.index(name)
                change = rank_earlier - rank_now
                if change > 0:
                    change = '+' + str(change)
            except:
                change = 'new colony'
            row = [name, colony['population'], colony['gdp'], change, '']
            table.append(row)
        return table

    def get_latest(self):
        latest = self.data[-1]
        try:
            before = self.data[-2]
        except:
            before = None
        return latest, before

    def format_forum(self):
        def format_header(line):
            return '     ' + ' | '.join(line)

        def format_divider():
            lengths = self.get_max_lengths()
            return '-' * (lengths[0] + 5) + ' | ' + '-' * (lengths[1]) + ' | ' + '-' * (lengths[2]) + ' | ' + '-' * (
                lengths[3]) + ' | ' + '-' * (lengths[4])

        def format_row(rank, line):
            name_field = '[url={}]{}[/url]'.format(self.get_colony_url(line[0].strip()), line[0].strip()) + (len(
                    line[0]) - len(line[0].strip())) * ' '
            whole_row = [rank] + [name_field] + line[1:]
            return ' | '.join(whole_row)

        def pop_change(name):
            change = 0
            for colony in self.latest['colonies']:
                if colony['name'] == name:
                    change += int(colony['population'])
            for colony in self.earlier['colonies']:
                if colony['name'] == name:
                    change -= int(colony['population'])
            return change

        self.write('[font=Courier new]')
        self.write(format_header(self.formatted_table[0]))
        self.write(format_divider())
        for i, line in enumerate(self.formatted_table[1:]):
            i += 1
            name = line[0].strip()
            length = len(str(i))
            rank = str(str(i) + ' ' * (2 - length))
            if '+' in line[3]:
                line[3] = '[color=#008000]' + ' ' + line[3] + '[/color]'
            elif '-' in line[3]:
                line[3] = '[color=#ffa500]' + ' ' + line[3] + '[/color]'
            if i == 1:
                line[-1] = '[color=#6633ff]Holds the Crown and can request an extra bonus per week![/color]'
            elif pop_change(name) > 0:
                line[-1] = '[color=#336633]Growing Well[/color]'
            elif pop_change(name) <= -100:
                line[-1] = '[color=#cc3333]Growing Slowly[/color]'
            else:
                line[-1] = '[color=#336633]Growing[/color]'
            self.write(format_row(rank, line))
            if i + 1 > self.table_length: break
        self.write('[/font]')
        self.write('SOC currently has an RRR of {}.'.format(self.latest['rrr']))

    def format_table(self):
        new_table = []
        max_lengths = self.get_max_lengths()
        headers = self.raw_table[0]
        new_headers = []
        for i, header in enumerate(headers):
            difference = max_lengths[i] - len(header)
            start = int(difference / 2)
            new_header = start * ' ' + header + (difference - start) * ' '
            new_headers.append(new_header)
        rows = self.raw_table[1:]
        for row in rows:
            new_row = []
            for i, cell in enumerate(row):
                length = len(str(cell))
                if i == 0 or i == 4:
                    new_cell = str(cell) + (max_lengths[i] - length) * ' '
                else:
                    new_cell = (max_lengths[i] - length) * ' ' + str(cell)
                new_row.append(new_cell)
            new_table.append(new_row)
        new_table.insert(0, new_headers)
        return new_table

    def get_max_lengths(self):
        column_lengths = []
        for i in range(len(self.raw_table[0])):
            column = []
            for row in self.raw_table:
                column.append(len(str(row[i])))
            column_lengths.append(max(column))
        return column_lengths

    def remove_file(self):
        if os.path.isfile(self.write_file):
            os.remove(self.write_file)

    def write(self, line):
        with open(self.write_file, 'a') as file:
            file.write(line)
            file.write('\n')


def main():
    soc_charter_code = 'DOOb6Emc'
    Downloader(soc_charter_code)
    Table(soc_charter_code, table_length=10).format_forum()


if __name__ == '__main__':
    main()