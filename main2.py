import os
import time

import requests
from bs4 import BeautifulSoup

import utilities as util
import pandas as pd

class Scraper:
    """
    Class to handle downloading, parsing and storing the data for an entire commonwealth.
    """

    def __init__(self, charter_code):
        self.charter_code = charter_code
        self.database_file = '{}.json'.format(self.charter_code)

        self.html = self.download_html()
        self.new_data = self.parse_html(self.html)
        self.save_data(self.new_data)


    def download_html(self):
        """
        Downloads the raw html from the commonwealth's website.
        :return: [str]
        """
        response = requests.get(util.get_url(self.charter_code))
        if response.status_code != 200:
            raise ConnectionError('Could no reach {}.'.format(response.url))
        return response.text

    def parse_html(self, html):
        """
        Extracts all useful data from the html source.
        :param html: [str]
        :return: [dict]
        """
        soup = BeautifulSoup(html, 'lxml')

        # head colony data
        # ---------------------------
        head_colony_html = soup.find('div', {'id': 'colonyHeadingMeta'})
        head_colony_soup = BeautifulSoup(str(head_colony_html), 'lxml')

        head_colony = dict(
            name= head_colony_soup.find('h2').text,
            founded= head_colony_soup.find('div', {'id': 'colonyHeadingFounding'}).text[9:],
            population= util.strip_text(head_colony_soup.find('div', {'id': 'colonyHeadingPopulation'}).text[12:])
        )

        # common wealth area
        # ------------------------
        cw_area_html = soup.find('div', {'id': 'commonWealthArea'})
        cw_area_soup = BeautifulSoup(str(cw_area_html), 'lxml')

        total_stats = cw_area_soup.find_all('div', {'class': 'commonwealthStat'})
        total_gdp = util.strip_text(total_stats[0].text[11:])
        total_pop = util.strip_text(total_stats[1].text[18:])

        # colonies
        # ------------------------
        html_colonies = cw_area_soup.find_all('div', {'class': 'commonwealthItem'})

        colonies_data = []
        for colony in html_colonies:
            colony_soup = BeautifulSoup(str(colony), 'lxml')
            colony_data = dict(
                    name=colony_soup.find('div', {'class', 'commonwealthItemName'}).text,
                    charter_code=colony_soup.find('a', href=True)['href'][10:-1],
                    population=util.strip_text(
                            colony_soup.find_all('div', {'class': 'commonwealthItemPopulation'})[0].text[11:]),
                    gdp=util.strip_text(
                            colony_soup.find_all('div', {'class': 'commonwealthItemPopulation'})[1].text[5:])
            )
            colonies_data.append(colony_data)

        # RRR
        # ------------------------
        RRR = cw_area_soup.find('div', {'id': 'rrrIndexArea'}).text[12:].strip()

        # merge data
        # ------------------------
        commonwealth_data = dict(
                head_colony=head_colony,
                total_gdp=total_gdp,
                total_population=total_pop,
                RRR=RRR,
                colonies=colonies_data,
                timestamp=int(time.time())
        )
        return commonwealth_data

    def save_data(self, new_data):
        """
        Appends the new data to the existing .json file.
        Creates a new file if none exists.
        :param new_data: [dict]
        :return: None
        """
        if os.path.isfile(self.database_file):
            save_data = util.load_json(self.database_file)
        else:
            save_data = []
        save_data.append(new_data)
        util.save_json(save_data, self.database_file)





class DataFrame:
    def __init__(self):
        data = util.load_json('DOOb6Emc.json')[-1]['colonies']
        df = pd.DataFrame(data=data)

        #shift and drop the first row to fix he index starting at 1
        df = df.shift(periods=1, axis=0)
        df = df.drop(df.index[[0]])
        df = df[['name', 'population', 'gdp']]
        print(df.head(5))



if __name__ == '__main__':
    charter_code = 'DOOb6Emc'
    scrape = Scraper(charter_code)
    scrape.parse_html(scrape.download_html())


    DataFrame()