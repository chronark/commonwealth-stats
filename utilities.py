import json
import logging

def load_json(filename):
    with open(filename, 'r') as file:
        return json.load(file)


def save_json(json_data, filename):
    with open(filename, 'w') as file:
        json.dump(json_data, file, indent=4, sort_keys=True)


def get_url(charter_code):
    return 'https://www.my-colony.com/colonies/{}'.format(charter_code)

def strip_text(text):
    return text.replace(',', '').replace('$', '')

def get_logger():
    logger = logging.getLogger(__name__)

    logging.getLogger("requests").setLevel(logging.CRITICAL)
    logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s')

    logger.setLevel('INFO')
    return logger


log = get_logger()